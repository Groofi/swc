from django.views.generic import CreateView

from user.forms import CustomUserForm
from user.models import CustomUser
from user.serializers import CustomUserSerializer
from rest_framework import viewsets


class Registration(CreateView):
    template_name = 'registration/registration.html'
    form_class = CustomUserForm
    success_url = '/'


class CustomUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
