from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models


class CustomUser(AbstractUser):
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['password', 'first_name', 'last_name']
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        verbose_name='Логин',
        max_length=150,
        unique=True,
        validators=[username_validator],
        error_messages={"unique": "Значение должно быть уникальным"},
    )
    password = models.CharField(verbose_name='Пароль', max_length=150)
    first_name = models.CharField(verbose_name="Имя", max_length=150)
    last_name = models.CharField(verbose_name="Фамилия", max_length=150)
    birthday = models.DateField(verbose_name="День рождения", blank=True, null=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
