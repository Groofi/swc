from django import forms

from user.models import CustomUser


class CustomUserForm(forms.ModelForm):
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput, required=True)

    class Meta:
        model = CustomUser
        fields = ('username', 'password', 'first_name', 'last_name', 'birthday')

    def clean_password(self):
        password = self.cleaned_data.get("password")
        return password

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])

        if commit:
            user.save()
        return user
