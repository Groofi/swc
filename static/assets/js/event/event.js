function add_members(pk, user_id) {
    fetch(`/api/events/${pk}/`, {
        method: "GET",
        headers: {
            "X-Requested-With": "XMLHttpRequest",
        },
    })
        .then(response => response.json())
        .then(data => {
            fetch(`/api/events/${pk}/`, {
                method: "PUT",
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                },
                body: JSON.stringify({
                    'id': data.id,
                    'title': data.title,
                    'description': data.description,
                    'creator': data.creator,
                    'members': [...data.members, "http://127.0.0.1:8000/api/users/2/"]
                })
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    const todoList = document.getElementById(`nav-one-$event.id`);
                    todoList.innerHTML = "";

                    (data.context).forEach(todo => {
                        const todoHTMLElement = `
                <li>
                  <p>Task: ${todo.task}</p>
                  <p>Completed?: ${todo.completed}</p>
                </li>`
                        todoList.innerHTML += todoHTMLElement;
                    });
                })
        })

}