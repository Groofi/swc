from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response

from event.models import Event
from event.serializers import EventSerializer


@method_decorator(login_required, name='dispatch')
class Index(ListView):
    template_name = 'event/events.html'
    model = Event

    def get_queryset(self):
        return Event.objects.prefetch_related('creator', 'members').exclude(creator=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["creator_event"] = Event.objects.prefetch_related('creator', 'members'). \
            filter(creator=self.request.user)
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Index, self).dispatch(*args, **kwargs)


class EventsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({'error': None, 'result': serializer.data}, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response({'error': None, 'result': serializer.data})

    def destroy(self, request, *args, **kwargs):
        event = self.get_object()
        if event.creator.id == self.request.user.id:
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(data={'error': None, 'result': 'Успешно удалено'}, status=status.HTTP_204_NO_CONTENT)
        return Response(data={'error': 'Только создатель события может удалить событие', 'result': None},
                        status=status.HTTP_403_FORBIDDEN)
