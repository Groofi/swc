from django.db import models

from user.models import CustomUser


class Event(models.Model):
    title = models.CharField(max_length=150, verbose_name='Заголовок')
    description = models.TextField(verbose_name='Описание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    creator = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Создатель')
    members = models.ManyToManyField(CustomUser, related_name='members', verbose_name='Участники')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'Событии'
