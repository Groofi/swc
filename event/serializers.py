from rest_framework import serializers

from event.models import Event


class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = ['id', 'title', 'description', 'created_at', 'creator', 'members']
